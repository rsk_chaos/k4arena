#include <iostream>
#include <fstream>
#include <set>
#include <vector>
#include <cstdlib>

using namespace std;

typedef int card;
typedef set<card> deck;

deck full_deck({2,3,4,5,6,7,8,9,10,11,12,13,14});

std::ostream & operator << (std::ostream &ss, deck const &x){
  for (auto y : x) ss << y << " ";
  return ss;
}

// input stream ops
std::istream & operator >> (std::istream &ss, deck &x){
  int n;
  ss >> n;
  for (int i = 0; i < n; i++){
    card c;
    ss >> c;
    x.insert(c);
  }

  return ss;
}

std::istream & operator >> (std::istream &ss, vector<deck> &x){
  int n;
  ss >> n;
  x.resize(n);
  for (int i = 0; i < n; i++){
    ss >> x[i];
  }

  return ss;
}

int sum(deck d){
  int s = 0;
  for (auto x : d) s += x;
  return s;
}

card get(deck d, int idx){
  auto i = d.begin();
  for (int c = 0; c < idx; c++) i++;
  return *i;
}

card bot_move(int id, vector<deck> decks, deck available, deck on_table){
  deck my_cards = decks[id];
  int idx = rand()%my_cards.size();
  card res = get(my_cards, idx);
  return res;
}

int main(int argc, char **argv){
  if (argc != 2){
    cout << "usage: " << argv[0] << " comfile" << endl;
    exit(-1);
  }

  fstream f(argv[1]);
  vector<deck> decks;
  int id;
  deck available; 
  deck on_table;

  srand(time(0));

  f >> id >> decks >> available >> on_table;
  f.close();

  // cout << "id: " << id << endl;
  // cout << "decks: " << endl;
  // for (auto x : decks) cout << x << endl;
  // cout << "available: " << available << endl;
  // cout << "on table: " << on_table << endl;
  card r = bot_move(id, decks, available, on_table);
  // cout << "result: " << r << endl;

  f.open(argv[1]);
  f << r << endl;
  f.close();
  return r;
}
