cflags=-ggdb -std=c++11
cc=g++

all: k4bot k4arena

%: %.cpp
	$(cc) $(cflags) $*.cpp -o $*

clean:
	rm k4bot k4arena
