#include <iostream>
#include <fstream>
#include <sstream>
#include <set>
#include <vector>
#include <cstdlib>
#include <functional>
#include <cstring>

#define COMFILE "k4com.txt"

using namespace std;

typedef int card;
typedef set<card> deck;

deck full_deck({2,3,4,5,6,7,8,9,10,11,12,13,14});

string print_deck(deck &d){
  stringstream s;
  for (auto y : d) s << y << " ";
  return s.str();
}

std::ostream & operator << (std::ostream &ss, deck const &x){
  ss << x.size() << " ";
  for (auto y : x) ss << y << " ";
  ss << endl;
  return ss;
}

int sum(deck d){
  int s = 0;
  for (auto x : d) s += x;
  return s;
}

card get(deck d, int idx){
  auto i = d.begin();
  for (int c = 0; c < idx; c++) i++;
  return *i;
}

card bot_move(string bot, int id, vector<deck> &decks, deck &available, deck &on_table){
  card r;
  fstream f(COMFILE, ios_base::out);

  if (!f.is_open()){
    cout << "error: failed to open comfile: " << COMFILE << endl;
    exit(-1);
  }

  f << id << endl;
  f << decks.size() << endl;
  for (auto &d : decks) f << d;
  f << available;
  f << on_table;

  f.close();

  system(("./" + bot + " " + COMFILE).c_str());

  f.open(COMFILE);
  f >> r;
  f.close();
  return r;
}

card human_move(deck *my_cards, deck on_table){
  card c = -1;

  while (!my_cards -> count(c)){
    cout << "chose a card. On table: " << print_deck(on_table) << ", you have " << print_deck(*my_cards) << "." << endl;
    cin >> c;
    if (!my_cards -> count(c)) cout << "invalid: " << c << endl;
  }
  return c;
}

int main(int argc, char **argv){
  deck buy_deck = full_deck;
  deck on_table;
  int n = argc-1;

  // player data
  vector<deck> decks(n);
  vector<int> scores(n);
  vector<function<card(void)> > contestants(n);

  if (argc < 2){
    cout << "usage: " << argv[0] << " cmd_player1 cmd_player2 ... (use cmd_playerX=human to insert a human player)" << endl;
    return 0;
  }

  // initialize player functions
  for (int i = 0; i < n; i++){
    decks[i] = full_deck;

    if (!strcmp(argv[i+1], "human")){
      // a human player
      contestants[i] = [i, &decks, &on_table] () -> card {
	return human_move(&decks[i], on_table);
      };
    }else{
      // a bot player with command argv[i+1]
      int id = i;
      string bot = argv[i+1];
      contestants[i] = [bot, id, &decks, &buy_deck, &on_table] () -> card {
	return bot_move(bot, id, decks, buy_deck, on_table);
      };
    }
  }

  srand(time(0));

  while (!buy_deck.empty()){
    int idx = rand()%buy_deck.size();
    bool done = false;
    on_table.clear();

    while (!done){
      card c;
      c = get(buy_deck, idx);
      buy_deck.erase(c);
      on_table.insert(c);

      // check who (if anyone) wins the bet
      int best = -1;
      int second = -1;
      int highest = -1;
      for (int i = 0; i < n; i++) {
	card pc = contestants[i]();

	if (!decks[i].count(pc)){
	  cout << "player " << argv[i+1] << " tried to play an illegal card: " << pc << endl;
	  exit(-1);
	}else{
	  decks[i].erase(pc);
	}

	cout << "player " << argv[i+1] << " selects " << pc << endl;

	if (pc >= highest){
	  second = highest;
	  highest = pc;
	  best = i;
	}
      }

      if (highest > second){
	scores[best] += sum(on_table);
	cout << argv[best+1] << " takes " << print_deck(on_table) << endl;
	done = true;
      }
    }    
  }

  for (int i = 0; i < n; i++) cout << "player " << argv[i+1] << " score: " << scores[i] << endl;

  return 0;
}
